const db = require('../model/taskModel');
const shortId = require('shortid');

class TaskController {


async all(req, res){

  const sqlSelect = "SELECT * FROM shorturl";
  db.query(sqlSelect, (err, result) => {
    
    return res.status(200).json({result});   
  });	
    
}  


async create(req, res){
	
	const nameUrl = req.body.fullUrl;  
  const newUrl = shortId.generate();
  
  if (newUrl < 5 || newUrl > 10 ) {
    const newUrl = shortId.generate();
  }
 
  const dataUso = new Date().setDate(new Date().getDate() + 1);
  const sqlInsert = "INSERT INTO shorturl (url, urlCurta, validade, data) VALUE (?,?,?,NOW())";  
  db.query(sqlInsert, [nameUrl, newUrl,new Date(dataUso)], (err, result)=> {   
   return res.status(200).json({newUrl: "http://localhost:" + global.porta + "/task/" + newUrl});
  });

};


async show(req, res){
	
	const shortid = req.params.urlid;  

  const sqlSelect = "SELECT * FROM shorturl WHERE urlcurta = (?)";
  db.query(sqlSelect, [shortid], (err, result) => {
    if (result != false) {      
      return res.status(200).json(result[0]['url']);
    }else{
      return res.status(400).json({error:'HTTP 404'});
    }   
  });	
 
}

}

module.exports = new TaskController();