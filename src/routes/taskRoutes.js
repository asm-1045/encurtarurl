const express = require('express');
const router = express.Router(); 

const TaskController = require('../controller/TaskController');

/* Traz todas as urls da base  
*http://localhost:8081/task/
*/
router.get('/', TaskController.all);

/* Realiza a inserção na base de dados 
* http://localhost:8081/task/encurtador
*/
router.post('/encurtador', TaskController.create);

/*Redirecionamento url 
*http://localhost:8081/task/Fvu1NAEt2
*/
router.get('/:urlid', TaskController.show);


module.exports = router;
