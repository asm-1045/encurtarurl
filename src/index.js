const express = require('express');
const app = express();
const bodyParser = require('body-parser');

app.use(express.json());
app.use(bodyParser.urlencoded({extended: false}));

const TaskRoutes = require('./routes/taskRoutes');
app.use('/task', TaskRoutes); 

global.porta = 8081;
app.listen(global.porta, () =>{
 console.log('Api Online');
});

