-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 13-Jan-2021 às 15:11
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `encurtador_url`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `shorturl`
--

CREATE TABLE `shorturl` (
  `id` int(11) NOT NULL,
  `url` varchar(20) NOT NULL,
  `urlcurta` varchar(10) NOT NULL,
  `data` datetime NOT NULL,
  `validade` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `shorturl`
--

INSERT INTO `shorturl` (`id`, `url`, `urlcurta`, `data`, `validade`) VALUES
(18, 'http://uol.net', 'LHfb04E0b', '2021-01-12 00:43:23', '2021-01-13 03:43:23'),
(19, 'http://wisereducacao', 'F3qKZ1hQq', '2021-01-12 09:28:22', '2021-01-13 12:28:22'),
(20, 'http://uol.net.com', '2j5xtS6lV', '2021-01-12 09:31:37', '2021-01-13 12:31:37'),
(28, 'http://gmail.com', '838kWqEba', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'http://gmail.com', 'K1HVBmn9g', '2021-01-13 11:08:48', '2021-01-14 14:08:48'),
(30, 'http://gmail.com', 'LfHSxzx13', '2021-01-13 11:09:23', '2021-01-14 14:09:23');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `shorturl`
--
ALTER TABLE `shorturl`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `shorturl`
--
ALTER TABLE `shorturl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
